import { API } from "./API"

export const fetch_all_articles = async() => {
    try {
        const result = await API.get("/articles")
        return result.data.data
    } catch (error) {
        console.log("fetch_all_articles error:", error);
    }
}
export const fetchArticleById = async (id) => {
    let response = await API.get('/articles/' + id)
    return response.data.data
}

export const updateArticleById = async (id, newArticle) => {
    let response = await API.patch('/articles/' + id, newArticle)
    return response.data.message
}

export const deleteArticle = async (id) => {
    let response = await API.delete('/articles/' + id)
    return response.data.message
}

export const postArticle = async (article) => {
    let response = await API.post('/articles', article)
    return response.data.message
}

export const uploadImage = async (file) => {

    let formData = new FormData()
    formData.append('image', file)

    let response = await API.post('images', formData)
    return response.data.url
}