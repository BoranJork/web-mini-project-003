import axios from "axios"

export const fetchPage =async(page)=>{
    let response = await axios.get(`http://110.74.194.124:3034/api/articles?page=${page}&size=3`)
    return response.data
}