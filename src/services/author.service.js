import { API } from "./API"

export const fetch_all_author = async() => {
    try {
        const result = await API.get("/author")
        return result.data.data
    } catch (error) {
        console.log("fetch_all_author error:", error);
    }
}

export const fetchAuthorById = async (id) => {
    let response = await API.get('/author/' + id)
    return response.data.data
}

export const updateAuthorById = async (id, newAuthor) => {
    let response = await API.put('/author/' + id, newAuthor)
    return response.data.message

}
export const deleteAuthor = async (id) => {
    let response = await API.delete('/author/' + id)
    return response.data.message
}

export const postAuthor = async (author) => {
    let response = await API.post('/author', author)
    return response.data.message
}
export const uploadImage = async (file) => {

    let formData = new FormData()
    formData.append('image', file)

    let response = await API.post('images', formData)
    return response.data.url
}
