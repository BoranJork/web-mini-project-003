import { fetch_all_category } from "../../services/category.service"


const FETCH_ALL_CATEGORY ="FETCH_ALL_CATEGORY"
export const fetchAllCategory =()=>{
    return async dispatch=>{
        const result = await fetch_all_category()
        dispatch({
            type:FETCH_ALL_CATEGORY,
             payload: result
        })
    }
}