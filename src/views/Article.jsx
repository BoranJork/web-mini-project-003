
import React,{useState,useEffect} from "react";
import { Col, Container,DropdownButton,Dropdown,ButtonGroup, Row,Form,Button } from "react-bootstrap";
import { useParams } from "react-router";
import { useDispatch, useSelector } from 'react-redux'
import { fetchAllAuthor} from '../redux/actions/authorAction'
import { fetchArticleById, postArticle, updateArticleById, uploadImage } from "../services/articles.services";


export default function Article() {


  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png')
  const [imageFile, setImageFile] = useState(null)
  const clearTitle = React.useRef()
  const clearDescription = React.useRef()
  const {id} = useParams()


  
  const dispatch = useDispatch()
  const {author} = useSelector(state=>state.authorReducer)
  useEffect(()=>{
      dispatch(fetchAllAuthor())
  },[])


  useEffect(() => {

    if(id){
      fetchArticleById(id).then(article=>{
        setTitle(article.title)
        setDescription(article.description)
        setImageURL(article.image)
      })
    }

  }, [])


  const onAdd = async(e)=>{
      e.preventDefault()
      let article = {
          title,description
      }
      

      if(imageFile){
         let url = await uploadImage(imageFile)
         article.image = url
      }
      postArticle(article).then(message=>alert(message))
      clearTitle.current.value="";
      clearDescription.current.value="";
  }

  const onUpdate = async(e)=>{
    e.preventDefault()
    let article = {
        title,description
    }
    if(imageFile){
       let url = await uploadImage(imageFile)
       article.image = url
    }
    updateArticleById(id,article).then(message=>alert(message))
    clearTitle.current.value="";
    clearDescription.current.value="";
}
    return (
        <div>
            
    <Container>
      <h1 className="my-2">{id?"Update Article":"Add Article"}</h1>
      <Row>
        <Col md={8}>
          <Form>
            <Form.Group controlId="title">
              <Form.Label><b>Title</b></Form.Label>
              <Form.Control 
              ref={clearTitle}
                type="text" 
                placeholder="Title" 
                value={title}
                onChange={(e)=>setTitle(e.target.value)}
                />
              <Form.Text className="text-muted">
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="title">
              <Form.Label><b>Author</b></Form.Label>
           

             

            <select class="form-select" aria-label="Default select example">
            {
              author.map((item,index)=>
             
              <option value="{idex++}">{item.name}</option>
              )
            }
            </select>
            </Form.Group>
            

            <Form.Group controlId="description">
              <Form.Label><b>Description</b></Form.Label>
              <Form.Control 
               ref={clearDescription}
                as="textarea" 
                rows={4} 
                placeholder="Description" 
                value={description}
                onChange={(e)=>setDescription(e.target.value)}
              />
              
            </Form.Group>
            <Button 
                className="mt-2"
                variant="primary" 
                type="submit"
                onClick={id? onUpdate:onAdd}
            >
              {id?'Save':'Add'}
            </Button>
          </Form>
        </Col>
        <Col md={4}>
            <img className="w-100" src={imageURL}/>
            <Form>
            <Form.Group>
                <Form.File 
                    id="img" 
                    label="Choose Image" 
                    onChange={(e)=>{
                        let url = URL.createObjectURL(e.target.files[0])
                        setImageFile(e.target.files[0])
                        setImageURL(url)
                    }}
                    />
            </Form.Group>
            </Form>
        </Col>
      </Row>
    </Container>
        </div>
    )
}
